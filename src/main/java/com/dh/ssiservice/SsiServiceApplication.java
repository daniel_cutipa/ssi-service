package com.dh.ssiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//este es para especificar componentes de otra paqueteria
//@ComponentScan(basePackages = {"com.dh.ssiservice"})
@SpringBootApplication
public class SsiServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsiServiceApplication.class, args);
    }
}
