package com.dh.ssiservice.controller;


import com.dh.ssiservice.dao.IncidentRegistryCommand;
import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.services.AreaService;
import com.dh.ssiservice.services.IncidentRegistryService;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/incidentRegistries")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class IncidentRegistryController {
    private IncidentRegistryService service;
    private IncidentService incidentService;
    private AreaService areaService;

    public IncidentRegistryController(IncidentRegistryService service, IncidentService incidentService, AreaService areaService) {
        this.service = service;
        this.incidentService = incidentService;
        this.areaService = areaService;
    }

    @GET
    public Response getIncidentRegistries() {
        List<IncidentRegistryCommand> incidentRegistryList = new ArrayList<>();
        service.findAll().forEach(incidentRegistry -> {
            incidentRegistryList.add(new IncidentRegistryCommand(incidentRegistry));
        });
        return Response.ok(incidentRegistryList).build();
    }

    @GET
    @Path("/{id}")
    public Response getTrainingById(@PathParam("id") long id) {
        IncidentRegistry incidentRegistry = service.findById(id);
        return Response.ok(new IncidentRegistryCommand(incidentRegistry)).build();

    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public Response addIncidentRegistry(IncidentRegistryCommand incidentRegistryCommand) {
        IncidentRegistry model = incidentRegistryCommand.toDomain();
        model.setIncident(incidentService.findById(incidentRegistryCommand.getIncidentId()));
        model.setArea(areaService.findById(incidentRegistryCommand.getAreaId()));
        IncidentRegistry incidentRegistry = service.save(model);
        return Response.ok(new IncidentRegistryCommand(incidentRegistry)).build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteIncidentRegistry(@PathParam("id") long id) {
        service.deleteById(id);
    }

}
