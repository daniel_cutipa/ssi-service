package com.dh.ssiservice.controller;

import com.dh.ssiservice.repositories.SubCategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/subcategories")
public class SubCategoryController {
    private SubCategoryRepository subCategoryRepository;

    public SubCategoryController(SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    @RequestMapping
    public String getSubCategories(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("subCategories", StringUtils.isEmpty(code) ? subCategoryRepository.findAll() : subCategoryRepository.findByCode(code).get());
        return "subCategories";
    }

    public String getSubCtegoryById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("subCategory", subCategoryRepository.findById(id).get());
        return "subCategory";
    }
}
