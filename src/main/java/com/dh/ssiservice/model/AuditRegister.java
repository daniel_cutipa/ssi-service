/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import java.util.Date;

//@Entity
public class AuditRegister extends ModelBase {
    private String tipo;
    private Date date;
    private String norma;
    //private List<Employee> employees;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNorma() {
        return norma;
    }

    public void setNorma(String norma) {
        this.norma = norma;
    }
/*
    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
 */
}
