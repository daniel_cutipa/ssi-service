package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class Comunication extends ModelBase {
    private String tipo;
    private String value;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
