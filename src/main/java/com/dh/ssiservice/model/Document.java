package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class Document extends ModelBase {
    private String tipo;
    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
