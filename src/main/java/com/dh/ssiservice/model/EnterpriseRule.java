/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import java.util.List;

//@Entity
public class EnterpriseRule extends ModelBase {
    private String text;

    private List<Position> positions;
    private String internalAudit;
    private String externalAudit;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public String getInternalAudit() {
        return internalAudit;
    }

    public void setInternalAudit(String internalAudit) {
        this.internalAudit = internalAudit;
    }

    public String getExternalAudit() {
        return externalAudit;
    }

    public void setExternalAudit(String externalAudit) {
        this.externalAudit = externalAudit;
    }
}
