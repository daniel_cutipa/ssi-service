/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Incident extends ModelBase {
    private String name;
    private String code;
    @OneToOne(optional = false)
    private SubCategory subCategory;
    //@OneToMany(mappedBy = "incident", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    //private List<IncidentRegistry> incidentRegistries = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }
}
