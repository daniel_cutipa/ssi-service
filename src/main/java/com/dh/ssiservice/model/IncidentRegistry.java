package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class IncidentRegistry extends ModelBase {
    @OneToOne(optional = false)
    private Incident incident;
    private Date date;
    //@OneToMany(mappedBy = "incidentRegistry", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    //private List<Employee> employees;
    @OneToOne(optional = false)
    private Area area;
    private String causa;
    private String infracciones;
    private Double cuantific;

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /*
        public List<Employee> getEmployees() {
            return employees;
        }

        public void setEmployees(List<Employee> employees) {
            this.employees = employees;
        }
    */
    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getCausa() {
        return causa;
    }

    public void setCausa(String causa) {
        this.causa = causa;
    }

    public String getInfracciones() {
        return infracciones;
    }

    public void setInfracciones(String infracciones) {
        this.infracciones = infracciones;
    }

    public Double getCuantific() {
        return cuantific;
    }

    public void setCuantific(Double cuantific) {
        this.cuantific = cuantific;
    }

}
