/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import java.util.List;

//@Entity
public class Role extends ModelBase {
    private List<Position> positions;
    private List<Task> tasks;

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
