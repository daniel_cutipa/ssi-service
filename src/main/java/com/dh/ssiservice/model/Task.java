/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import java.util.List;

//@Entity
public class Task extends ModelBase {
    private String name;
    private List<String> instructions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<String> instructions) {
        this.instructions = instructions;
    }
}
