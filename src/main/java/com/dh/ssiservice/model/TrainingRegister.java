/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

//@Entity
public class TrainingRegister extends ModelBase {
    private Employee employee;
    private Training training;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
}
