package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Area;

public interface AreaService extends GenericService<Area> {
}
