package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.repositories.AreaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AreaServiceImpl extends GenericServiceImpl<Area> implements AreaService {
    private AreaRepository repository;

    public AreaServiceImpl(AreaRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Area, Long> getRepository() {
        return repository;
    }
}
